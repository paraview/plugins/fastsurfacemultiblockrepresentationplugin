/*=========================================================================

  Program:   ParaView
  Module:    vtkFastSurfaceMultiBlockRepresentation.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkFastSurfaceMultiBlockRepresentation.h"

#include "vtkAlgorithmOutput.h"
#include "vtkDataObject.h"
#include "vtkExtractBlock.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkObjectFactory.h"
#include "vtkPVTrivialProducer.h"
#include "vtkStreamingDemandDrivenPipeline.h"

vtkStandardNewMacro(vtkFastSurfaceMultiBlockRepresentation);

//----------------------------------------------------------------------------
vtkFastSurfaceMultiBlockRepresentation::vtkFastSurfaceMultiBlockRepresentation()
{
  // Superclass constructor instanciates another class for GeometryFilter.
  // This deletes the previous instance to use an ExtractBlock filter instead.
  // Superclass destructor will delete it.
  this->GeometryFilter->Delete();
  this->GeometryFilter = vtkExtractBlock::New();
  this->GeometryFilter->AddObserver(vtkCommand::ProgressEvent, this,
    &vtkFastSurfaceMultiBlockRepresentation::HandleGeometryRepresentationProgress);

  this->SetupDefaults();
}

//----------------------------------------------------------------------------
void vtkFastSurfaceMultiBlockRepresentation::SetExtractBlockId(int id)
{
  vtkDebugMacro(<< this->GetClassName() << " (" << this << "): setting BlockId to " << id);
  if (this->BlockId != id)
  {
    this->BlockId = id;
    // Trigger a Render.
    this->MarkModified();
  }
}

//----------------------------------------------------------------------------
int vtkFastSurfaceMultiBlockRepresentation::RequestData(
  vtkInformation* request, vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
  this->GeometryFilter->SetInputConnection(this->GetInternalOutputPort());

  if (inputVector[0]->GetNumberOfInformationObjects() == 1)
  {
    vtkDataObject* inputDO = vtkDataObject::GetData(inputVector[0], 0);
    vtkMultiBlockDataSet* inputMB = vtkMultiBlockDataSet::SafeDownCast(inputDO);

    if (inputMB)
    {
      auto extractBlock = vtkExtractBlock::SafeDownCast(this->GeometryFilter);
      if (extractBlock)
      {
        extractBlock->RemoveAllIndices();
        extractBlock->AddIndex(this->BlockId);
      }
      vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
      if (inInfo->Has(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT()))
      {
        vtkAlgorithmOutput* aout = this->GetInternalOutputPort();
        vtkPVTrivialProducer* prod = vtkPVTrivialProducer::SafeDownCast(aout->GetProducer());
        if (prod)
        {
          prod->SetWholeExtent(inInfo->Get(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT()));
        }
      }
    }
    else
    {
      vtkNew<vtkMultiBlockDataSet> placeholder;
      this->GeometryFilter->SetInputDataObject(0, placeholder);
    }
  }
  else
  {
    vtkNew<vtkMultiBlockDataSet> placeholder;
    this->GeometryFilter->SetInputDataObject(0, placeholder);
  }

  // essential to re-execute geometry filter consistently on all ranks since it
  // does use parallel communication (see paraview/paraview#19963).
  this->GeometryFilter->Modified();
  this->MultiBlockMaker->Update();

  // Do not need to call Superclass implementation, but still need this one.
  return vtkPVDataRepresentation::RequestData(request, inputVector, outputVector);
}

//----------------------------------------------------------------------------
void vtkFastSurfaceMultiBlockRepresentation::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "BlockId: " << this->BlockId << endl;
}
