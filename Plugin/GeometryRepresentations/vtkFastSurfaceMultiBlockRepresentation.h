/*=========================================================================

  Program:   ParaView
  Module:    vtkFastSurfaceMultiBlockRepresentation.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkFastMultiBlockRepresentation_h
#define vtkFastMultiBlockRepresentation_h

#include "vtkGeometryRepresentationWithFaces.h"

#include "vtkRemotingViewsModule.h" // for export macro
#include "vtkSetGet.h"

/**
 * @class  vtkFastSurfaceMultiBlockRepresentation
 * @brief  This representation extracts a block from its input and use it as its output.
 *
 * It was designed for data formatted as OpenFOAM, where actual surfaces are directly
 * available in specific blocks.
 * So there is no need to load and compute surface from inner volume block.
 *
 * The current approach is only based on block indices. Only one block is extracted, so
 * surfaces are expected to live under the same subblock (id 2 by default),
 * as in this OpenFOAM structure:
 * ```
 * | id   | structure                                                                 |
 * | ---- | ----                                                                      |
 * | 0    | root                                                                      |
 * | 1    | - inner volume (Unstructured Grid)                                        |
 * | 2    | - surfaces (-> Selected Block, each child block will be used for display) |
 * | 3    |   - surface1 (polygonal mesh)                                             |
 * | 4    |   - surface2 (polygonal mesh)                                             |
 * | 5    |   - ...                                                                   |
 * | N    | - other skipped blocks (optional)                                         |
 * ```
 *
 */

class VTKREMOTINGVIEWS_EXPORT vtkFastSurfaceMultiBlockRepresentation
  : public vtkGeometryRepresentationWithFaces
{
public:
  static vtkFastSurfaceMultiBlockRepresentation* New();
  vtkTypeMacro(vtkFastSurfaceMultiBlockRepresentation, vtkGeometryRepresentationWithFaces);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  // Forwarded to GeometryFilter
  /**
   * Set the ID of the block containing the data to use for rendering.
   * Default is 2, the whole multiblock is used.
   */
  void SetExtractBlockId(int id);
  vtkGetMacro(BlockId, int);

protected:
  vtkFastSurfaceMultiBlockRepresentation();
  ~vtkFastSurfaceMultiBlockRepresentation() = default;

  int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*) override;

  int BlockId = 2;

private:
  vtkFastSurfaceMultiBlockRepresentation(const vtkFastSurfaceMultiBlockRepresentation&) = delete;
  void operator=(const vtkFastSurfaceMultiBlockRepresentation&) = delete;
};

#endif
