
This plugin adds a representation dedicated to showing surfaces of OpenFOAM datasets and other similarly formatted multiblock dataset.

Instead of computing the surface from each contained meshes, a block is extracted and is considered to be the actual surface of the dataset.
By default, the following multiblock structure is expected and block of flat index 2 (`surfaces`) will be used:
```
| id   | structure                                                                 |
| ---- | ----                                                                      |
| 0    | root                                                                      |
| 1    | - inner volume (Unstructured Grid)                                        |
| 2    | - surfaces (-> Selected Block, each child block will be used for display) |
| 3    |   - surface1 (polygonal mesh)                                             |
| 4    |   - surface2 (polygonal mesh)                                             |
| 5    |   - ...                                                                   |
| N    | - other skipped blocks (optional)                                         |
```

Two variants of the representation are available, `Fast Surface Mapper` and `Fast Surface With Edges Mapper`.

The id of the block to use is configurable through the settings (`Edit > Settings > FastRepresentation Plugin`). It can be adjusted for the current representation in the `Display` property panel.

For a full performance run, you should avoid the classic `Surface` and `Surface With Edges` representation. 
As `Surface` is often the default, you can change the Outline Threshold option, so newly loaded data will be presented as Outline.
Go to `Edit > Settings > Render View` and put the Outline Threshold to zero (or something small that you think is adequate for your use case).

You can now load your file, create a new View (new representations will not be available in the previous views).
You are now able to select one of the new representations from the dropdown menu in paraview.
